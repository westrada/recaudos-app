(function () {
    'use strict';

    angular
        .module('app.aplicacion.consultRequestPending')
        .config(consultRequestPendingConfig);

    function consultRequestPendingConfig($stateProvider) {

        $stateProvider
            .state('templateAuth.consultRequestPending', {
                url: "/consultRequestApproved",
                resolve: {
                    pendingRequests: function (RequestPendingService, $timeout, $state) {
                        return RequestPendingService.getRequestPending()
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                $timeout(function () {
                                    $state.go('template.login');
                                });
                            });
                    },
                },
                views: {
                    'contenido': {
                        templateUrl: "app/aplicacion/consultRequestPending/consultRequestPending.tmpl.html",
                        controller: "ConsultRequestPendingController",
                        controllerAs: 'vm'
                    }
                }
            });
    }
})();
