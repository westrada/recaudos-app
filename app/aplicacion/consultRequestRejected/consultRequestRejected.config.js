(function () {
    'use strict';

    angular
        .module('app.aplicacion.consultRequestRejected')
        .config(consultRequestRejectedConfig);

    function consultRequestRejectedConfig($stateProvider) {

        $stateProvider
            .state('templateAuth.consultRequestRejected', {
                url: "/consultRequestRejected",
                resolve: {
                    rejectedRequests: function (RequestRejectedService, $timeout, $state) {
                        return RequestRejectedService.getRequestRejected()
                            .then(function (response) {
                                return response.data;
                            }, function (error) {
                                $timeout(function () {
                                    $state.go('template.login');
                                });
                            });
                    },
                },
                views: {
                    'contenido': {
                        templateUrl: "app/aplicacion/consultRequestRejected/consultRequestRejected.tmpl.html",
                        controller: "ConsultRequestRejectedController",
                        controllerAs: 'vm'
                    }
                }
            });
    }
})();
