(function () {
    'use strict';

    angular
        .module('app.aplicacion.createAgreement')
        .factory('CreateAgreementService', CreateAgreementService);

    function CreateAgreementService($q, $http, API_BACKEND, UserService, $sessionStorage) {
        var typeRequest;
        var idAgreement;

        var service = {
            setTypeRequest: setTypeRequest,
            getTypeRequest: getTypeRequest,
            setIdAgreement: setIdAgreement,
            getIdAgreement: getIdAgreement,
            getUser: getUser
        };

        return service;

        //////////////////////////////////////

        function setTypeRequest(typeRequest) {
            this.typeRequest = typeRequest;
        }

        function getTypeRequest() {
            return this.typeRequest
        }

        function setIdAgreement(idAgreement) {
            this.idAgreement = idAgreement;
        }

        function getIdAgreement() {
            return this.idAgreement;
        }

        function getUser() {
            return $sessionStorage.user;
        }

    }
})();
