(function () {
    'use strict';

    angular
        .module('app.aplicacion.components', [
            'app.aplicacion.components.generalData',
            'app.aplicacion.components.dataBase',
            'app.aplicacion.components.outputFile',
            'app.aplicacion.components.accountsReceivable',
            'app.aplicacion.components.annexDocuments',
            'app.aplicacion.components.atm',
            'app.aplicacion.components.bankCorrespondent',
            'app.aplicacion.components.bbvaNetBbvaCash',
            'app.aplicacion.components.captureType',
            'app.aplicacion.components.financialTerminal',
            'app.aplicacion.components.mobileBanking',
            'app.aplicacion.components.paymentsPse',
            'app.aplicacion.components.payTime',
            'app.aplicacion.components.pinGeneration',
            'app.aplicacion.components.referenceInformation',
            'app.aplicacion.components.taxDispersionAccounts',
            'app.aplicacion.components.specialTaxParameters',
            'app.aplicacion.components.recaudoProductsTax'
        ]);
})();
