(function () {
    'use strict';

    angular
        .module('app.aplicacion.components.captureType')
        .controller('CaptureTypeController', CaptureTypeController);

    function CaptureTypeController(CreateAgreementService) {
        var vm = this;

        vm.typeRequest = CreateAgreementService.getTypeRequest();

        vm.changeType = changeType;
        vm.statusCodeEan = false;
        vm.statusCollectionCard = false;

        if (vm.typeRequest == 'R') {
            vm.types = [
                {
                    nombre: "Manual",
                    value: "MAN"
                },
                {
                    nombre: "Código de Barras",
                    value: "CBA"
                },
                {
                    nombre: "Manual y Código de Barras",
                    value: "MCB"
                },
                {
                    nombre: "Tarjeta de Recaudo",
                    value: "TJR"
                }
            ];
        } else if (vm.typeRequest == 'I') {
            vm.types = [
                {
                    nombre: "Manual",
                    value: "MAN"
                },
                {
                    nombre: "Código de Barras",
                    value: "CBA"
                },
                {
                    nombre: "Manual y Código de Barras",
                    value: "MCB"
                }
            ];
        }


        function changeType() {
            if (vm.type == '' || vm.type == 'MAN') {
                vm.statusCodeEan = false;
                vm.statusCollectionCard = false;
            } else if (vm.type == 'CBA' || vm.type == 'MCB') {
                vm.statusCollectionCard = false;
                vm.statusCodeEan = true;
            } else if (vm.type == 'TJR') {
                vm.statusCodeEan = true;
                vm.statusCollectionCard = true;
            }
        }
    }
})();
