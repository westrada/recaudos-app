(function () {
    'use strict';

    angular
        .module('app.aplicacion.components.generalData')
        .controller('GeneralDataController', GeneralDataController);

    function GeneralDataController($scope, CreateAgreementService, GeneralDataService, toastr) {
        var vm = this;
        vm.typeRequest = CreateAgreementService.getTypeRequest();

        //inicialización
        vm.user = CreateAgreementService.getUser();
        vm.manager = vm.user.nombres + ' ' + vm.user.apellidos;
        vm.managerEmail = vm.user.email;
        vm.expirationDate = '2099-12-12';
        vm.stateContract = 'A';
        vm.inhouse = false;
        vm.massiveActive = false;
        vm.multilote = false;
        vm.consecutive = false;
        vm.startTime = new Date(1970, 0, 1, 3, 0, 1);
        vm.finalHour = new Date(1970, 0, 1, 3, 1, 1);
        vm.documentTraffic = false;
        vm.associatedAgreements = false;
        vm.ucw = false;

        vm.save = save;
        vm.stateIdentificacion = false;
        vm.stateIdentificacion = false;
        vm.stateVer = false;
        vm.changeIdentificationType = changeIdentificationType;
        vm.paymentMethodsSelected = [];
        vm.addPaymentMethod = addPaymentMethod;
        vm.deletePaymentMethodSelected = deletePaymentMethodSelected;
        vm.changeSubscriptionFormat = changeSubscriptionFormat;
        vm.addAgreement = addAgreement;
        vm.deleteAgreement = deleteAgreement;
        vm.listStatusAssociatedAgreements = [];
        var convenio = {
            associatedAgreementCode: '',
            numberAssociatedAgreement: '',
            associatedAccount: ''
        };

        vm.listStatusAssociatedAgreements.push(convenio);
        vm.statusAssociatedAgreements = false;
        vm.statusSubscriptionFormat = false;
        vm.statusUcw = false;
        vm.isTaxAndAdditional = false;

        if (vm.typeRequest == 'R') {
            vm.isCollection = true;
            vm.identificationTypes = [
                {
                    nombre: "Cédula",
                    value: "01"
                },
                {
                    nombre: "Cédula de Extranjería",
                    value: "02"
                },
                {
                    nombre: "NIT",
                    value: "03"
                }
            ];

            vm.accountTypes = [
                {
                    nombre: "Cuenta de Ahorros",
                    value: "AH"
                },
                {
                    nombre: "Cuenta Corriente",
                    value: "CC"
                },
                {
                    nombre: "Crédito Líquido",
                    value: "CL"
                }
            ];

            vm.typeAgreements = [
                {
                    nombre: "Recaudo Nacional",
                    value: "001"
                },
                {
                    nombre: "Servicios Públicos",
                    value: "002"
                },
                {
                    nombre: "Impuestos Distritales",
                    value: "003"
                }
            ];

            vm.classAgreements = [
                {
                    nombre: "000",
                    value: "000"
                }
            ];

            vm.descriptions = [
                {
                    nombre: "PAGO FACTURA POR DISTRIBUCION DE PRODUCTOS",
                    value: "PAGO FACTURA POR DISTRIBUCION DE PRODUCTOS"
                },
                {
                    nombre: "PAGO FACTURA POR PRESTACION DE SERVICIOS",
                    value: "PAGO FACTURA POR PRESTACION DE SERVICIOS"
                },
                {
                    nombre: "PAGO FACTURA SERVICIOS PUBLICOS",
                    value: "PAGO FACTURA SERVICIOS PUBLICOS"
                },
                {
                    nombre: "PAGO FACTURA CARTERA",
                    value: "PAGO FACTURA CARTERA"
                },
                {
                    nombre: "PAGO FACTURA MATRICULAS",
                    value: "PAGO FACTURA MATRICULAS"
                },
                {
                    nombre: "PAGO FACTURA CUOTAS PROYECTOS INMOBILIARIOS",
                    value: "PAGO FACTURA CUOTAS PROYECTOS INMOBILIARIOS"
                },
                {
                    nombre: "PAGO FACTURA ADMINISTRACION",
                    value: "PAGO FACTURA ADMINISTRACION"
                },
                {
                    nombre: "PAGO FACTURA TELEFONIA CELULAR",
                    value: "PAGO FACTURA TELEFONIA CELULAR"
                },
                {
                    nombre: "PAGO FACTURA TELEVISION POR CABLE",
                    value: "PAGO FACTURA TELEVISION POR CABLE"
                }
            ];

            vm.paymentMethods = [
                {
                    nombre: "Efectivo",
                    value: "EFE-Efectivo"
                },
                {
                    nombre: "Cheque Canje",
                    value: "CHC-Cheque canje"
                },
                {
                    nombre: "Cargo a cuenta",
                    value: "CAC-Cargo a cuenta"
                },
                {
                    nombre: "Cheque BBVA",
                    value: "CHB-Cheque BBVA"
                },
                {
                    nombre: "Tarjeta de Crédito",
                    value: "TJC-Tarjeta de crédito"
                },
                {
                    nombre: "Crédito Virtual",
                    value: "CRV-Crédito Virtual"
                }
            ];

            vm.subscriptionFormats = [
                {
                    nombre: "Detalle en Cuenta",
                    value: "DC"
                },
                {
                    nombre: "Abono Diario (Cta. de Espera)",
                    value: "AD"
                }
            ];

        } else if (vm.typeRequest == 'I') {
            vm.isCollection = false;
            vm.identificationTypes = [
                {
                    nombre: "Cédula",
                    value: "01"
                },
                {
                    nombre: "NIT",
                    value: "03"
                }
            ];

            vm.accountTypes = [
                {
                    nombre: "Cuenta de Ahorros",
                    value: "AH"
                },
                {
                    nombre: "Cuenta Corriente",
                    value: "CC"
                }
            ];

            vm.typeAgreements = [
                {
                    nombre: "Recaudos",
                    value: "001"
                },
                {
                    nombre: "Servicio Publico",
                    value: "002"
                },
                {
                    nombre: "Impuestos",
                    value: "003"
                },
                {
                    nombre: "Impuestos Nuevo",
                    value: "005"
                }
            ];

            vm.classAgreements = [
                {
                    nombre: "000",
                    value: "000"
                },
                {
                    nombre: "Departamental",
                    value: "002"
                }
            ];

            vm.descriptions = [
                {
                    nombre: "PAGO FACTURA POR DISTRIBUCION DE PRODUCTOS",
                    value: "PAGO FACTURA POR DISTRIBUCION DE PRODUCTOS"
                },
                {
                    nombre: "PAGO FACTURA POR PRESTACION DE SERVICIOS",
                    value: "PAGO FACTURA POR PRESTACION DE SERVICIOS"
                },
                {
                    nombre: "PAGO FACTURA SERVICIOS PUBLICOS",
                    value: "PAGO FACTURA SERVICIOS PUBLICOS"
                },
                {
                    nombre: "PAGO FACTURA CARTERA",
                    value: "PAGO FACTURA CARTERA"
                },
                {
                    nombre: "PAGO FACTURA MATRICULAS",
                    value: "PAGO FACTURA MATRICULAS"
                },
                {
                    nombre: "PAGO FACTURA CUOTAS PROYECTOS INMOBILIARIOS",
                    value: "PAGO FACTURA CUOTAS PROYECTOS INMOBILIARIOS"
                },
                {
                    nombre: "PAGO FACTURA ADMINISTRACION",
                    value: "PAGO FACTURA ADMINISTRACION"
                },
                {
                    nombre: "PAGO FACTURA TELEFONIA CELULAR",
                    value: "PAGO FACTURA TELEFONIA CELULAR"
                },
                {
                    nombre: "PAGO FACTURA TELEVISION POR CABLE",
                    value: "PAGO FACTURA TELEVISION POR CABLE"
                },
                {
                    nombre: "PAGO RECAUDO DE IMPUESTOS",
                    value: "PAGO RECAUDO DE IMPUESTOS"
                }
            ];

            vm.paymentMethods = [
                {
                    nombre: "Efectivo",
                    value: "EFE-Efectivo"
                },
                {
                    nombre: "Cheque Canje",
                    value: "CHC-Cheque canje"
                },
                {
                    nombre: "Cargo a cuenta",
                    value: "CAC-Cargo a cuenta"
                },
                {
                    nombre: "Cheque BBVA",
                    value: "CHB-Cheque BBVA"
                }
            ];

            vm.subscriptionFormats = [
                {
                    nombre: "Abono Diario (Cta. de Espera)",
                    value: "AD"
                }
            ];

        }



        vm.typeTaxs = [
            {
                nombre: "Vehiculo",
                value: "1"
            },
            {
                nombre: "Sobre Tasa Gasolina",
                value: "2"
            }
        ];

        vm.stateContracts = [
            {
                nombre: "Activo",
                value: "A"
            },
            {
                nombre: "Inactivo",
                value: "I"
            }
        ];

        function changeIdentificationType() {
            if (vm.identificationType != '') {

                if (vm.identificationType == '03') {
                    vm.stateVer = true;
                } else {
                    vm.stateVer = false;
                }

            } else {
                vm.stateVer = false;
            }
        }

        function addPaymentMethod() {
            angular.forEach(vm.paymentMethods, function (value, key) {
                if (vm.paymentMethod == value.value) {
                    var sw = true;
                    angular.forEach(vm.paymentMethodsSelected, function (value2, key2) {
                        if (value == value2) {
                            sw = false;
                        }
                    });
                    if (sw) {
                        vm.paymentMethodsSelected.push(value);
                    }
                }
            });
        }

        function deletePaymentMethodSelected(paymentMethod) {
            var n = '';

            for (var i = 0; i < vm.paymentMethodsSelected.length; i++) {
                if (paymentMethod == vm.paymentMethodsSelected[i]) {
                    n = i;
                }
            }

            if (n != '' || n == '0') {
                vm.paymentMethodsSelected.splice(n, 1);
            }
        }

        function changeSubscriptionFormat() {
            if (vm.subscriptionFormat == "AD") {
                vm.statusSubscriptionFormat = true;
                if (vm.typeRequest == 'I') {
                    vm.isTaxAndAdditional = true;
                } else {
                    vm.isTaxAndAdditional = false;
                }
            } else {
                vm.statusSubscriptionFormat = false;
                vm.isTaxAndAdditional = false;
            }
        }

        function addAgreement() {
            var convenio = {
                associatedAgreementCode: '',
                NumberAssociatedAgreement: '',
                associatedAccount: ''
            };
            vm.listStatusAssociatedAgreements.push(convenio);
        }

        function deleteAgreement(index) {
            if (vm.listStatusAssociatedAgreements.length > 1) {
                vm.listStatusAssociatedAgreements.splice(index, 1);
            } else {
                vm.listStatusAssociatedAgreements.splice(index, 1);
                vm.listStatusAssociatedAgreements.push({
                    associatedAgreementCode: '',
                    numberAssociatedAgreement: '',
                    associatedAccount: ''
                });
            }
        }

        $scope.$watch('vm.ucw', function (value) {
            if (vm.ucw == true) {
                vm.statusUcw = true;
            } else {
                vm.statusUcw = false;
            }
        });

        $scope.$watch('vm.associatedAgreements', function (value) {
            if (vm.associatedAgreements == true) {
                vm.statusAssociatedAgreements = true;
            } else {
                vm.statusAssociatedAgreements = false;
            }
        });

        function save() {

            var tacta = vm.accountNumber.length;
            var ceroCta = '';
            var accountNumber;

            if (tacta < 18) {
                for (var i = 0; i < (18 - tacta); i++) {
                    ceroCta = ceroCta + '0';
                }
            }

            accountNumber = ceroCta + vm.accountNumber;

            //eliminar esto a futuro.
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var hora = today.getHours();
            var min = today.getMinutes();
            var seg = today.getSeconds();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            if (hora < 10) {
                hora = '0' + hora;
            }

            if (min < 10) {
                min = '0' + min;
            }

            if (seg < 10) {
                seg = '0' + seg;
            }
            var today = yyyy + "-" + mm + "-" + dd + "T" + hora + ":" + min + ":" + seg;



            // hasta aqui eliminar

            var nametypeAgreement;
            var numberIdentification = '';

            angular.forEach(vm.typeAgreements, function (value, key) {
                if (vm.typeAgreement == value.value) {
                    nametypeAgreement = value.nombre;
                }
            });

            if (vm.identificationNumber.length < 15) {
                var t = 15 - vm.identificationNumber.length;
                for (var i = 0; i < t; i++) {
                    numberIdentification = numberIdentification + '0';
                }
                numberIdentification = vm.identificationType + numberIdentification + vm.identificationNumber;
            } else {
                numberIdentification = vm.identificationType + vm.identificationNumber;
            }

            if (vm.stateVer) {
                numberIdentification += vm.ver;
            }

            var relatedContract = [];

            // cuenta principal - centralizadora
            var relatedAccount = {
                "relatedContractId": 1,
                "contractId": 1,
                "number": accountNumber,
                "product": {
                    "id": accountNumber.substring(8, 10),
                    "name": vm.accountType
                },
                "relationType": {
                    "id": "CON",
                    "name": "Cuenta central"
                },
                "percentage": 0,
                "bankId": accountNumber.substring(0, 4)
            };
            relatedContract.push(relatedAccount);

            // cuenta de espera
            if (vm.subscriptionFormat == 'AD') {
                var waitingAccount = {
                    "relatedContractId": 2,
                    "contractId": 2,
                    "number": vm.waitingAccountNumber,
                    "product": {
                        "id": '19',
                        "name": 'CTA Espera'
                    },
                    "relationType": {
                        "id": "CTA",
                        "name": "Cuenta de espera"
                    },
                    "percentage": 0,
                    "bankId": vm.waitingAccountNumber.substring(0, 4)
                };
                relatedContract.push(waitingAccount);
            }

            // cuenta 0044
            if (vm.typeRequest == 'I' && vm.subscriptionFormat == 'AD') {
                var account0044 = {
                    "relatedContractId": 3,
                    "contractId": 3,
                    "number": vm.tax0044,
                    "product": {
                        "id": '19',
                        "name": vm.typeTax
                    },
                    "relationType": {
                        "id": "044",
                        "name": "0044"
                    },
                    "percentage": 0,
                    "bankId": vm.tax0044.substring(0, 4)
                };
                relatedContract.push(account0044);
            }

            var horaStartTime = vm.startTime.getHours();
            var minutesStartTime = vm.startTime.getMinutes();
            var secondStartTime = vm.startTime.getSeconds();

            if (vm.startTime.getHours() < 10) {
                horaStartTime = '0' + vm.startTime.getHours();
            }

            if (vm.startTime.getMinutes() < 10) {
                minutesStartTime = '0' + vm.startTime.getMinutes();
            }

            if (vm.startTime.getSeconds() < 10) {
                secondStartTime = '0' + vm.startTime.getSeconds();
            }
            var startTime = horaStartTime + ":" + minutesStartTime + ':' + secondStartTime;

            var horaFinalHour = vm.finalHour.getHours();
            var minutesFinalHour = vm.finalHour.getMinutes();
            var secondFinalHour = vm.finalHour.getSeconds();

            if (vm.finalHour.getHours() < 10) {
                horaFinalHour = '0' + vm.finalHour.getHours();
            }

            if (vm.finalHour.getMinutes() < 10) {
                minutesFinalHour = '0' + vm.finalHour.getMinutes();
            }

            if (vm.finalHour.getSeconds() < 10) {
                secondFinalHour = '0' + vm.finalHour.getSeconds();
            }
            var finalHour = horaFinalHour + ':' + minutesFinalHour + ':' + secondFinalHour;

            var stateIdAgrement = true;
            var idAgrement = '0010000000000';
            if (CreateAgreementService.getIdAgreement() != undefined) {
                idAgrement = CreateAgreementService.getIdAgreement();
                stateIdAgrement = false;
            }

            var requestAgreement = {
                "idAgreement": idAgrement,
                "agreementType": {
                    "id": "0010000000000",
                    "name": nametypeAgreement
                },
                "name": vm.agreementName,
                "agreementDescription": vm.description,
                "status": {
                    "id": "P",
                    "name": "Procesando",
                    "dateState": vm.expirationDate,
                    "statusType": {
                        "id": "P",
                        "name": "Procesando",
                        "dateStatusType": today
                    }
                },
                "agreementClass": {
                    "id": vm.typeRequest == 'R' ? '1' : '2',
                    "name": vm.typeRequest == 'R' ? 'REC' : 'IMP',
                    "agreementClassType": {
                        "id": vm.typeRequest == 'R' ? '1' : '2',
                        "name": vm.typeRequest == 'R' ? '001' : '002'
                    }
                },
                "RBMCode": "11",
                /* Verificar componente base de datos campo networkCode*/
                "relatedParticipants": [
                    {
                        "participantId": numberIdentification,
                        "participantTypeId": vm.identificationType,
                        "relationType": {
                            "id": "1",
                            "name": "cliente" //verificar esto 
                        }
                    },
                    {
                        "participantId": vm.manager,
                        "participantTypeId": 99,
                        "relationType": {
                            "id": "99",
                            "name": vm.manager
                        }
                    }
                ],
                "relatedContract": relatedContract,
                "EANCode": "0010000000000", // formulario de tipo de captura campo eancode
                "cards": [],
                "notifications": [
                    {
                        "notificationsType": {
                            "id": "03",
                            "name": "EMAIL  P"
                        },
                        "receiver": {
                            "receiverType": {
                                "id": "1",
                                "name": "Cliente"
                            },
                            "value": vm.managerEmail,
                            "name": vm.managerEmail
                        },
                        "typeReport": {
                            "id": "1",
                            "name": "",
                            "typeTax": {
                                "id": "1",
                                "name": "DEPARTAMENTAL"
                            }
                        },
                        "nameTax": ""
                    }
                ],
                "agreementConfiguration": {
                    "parameter": [],
                    "pin": [],
                    "channel": []
                },
                "horaInicio": startTime,
                "horaFin": finalHour
            };

            // ENVIO DE PAYMENT_METHOD
            var requestPaymentMethods = [];
            var namePaymentMethod = '';

            angular.forEach(vm.paymentMethodsSelected, function (value, key) {
                switch (value.value) {
                    case 'EFE-Efectivo':
                        namePaymentMethod = "CASH_PAYMENT";
                        break;
                    case 'CHC-Cheque canje':
                        namePaymentMethod = "EXCHANGE_CHECK";
                        break;
                    case 'CAC-Cargo a cuenta':
                        namePaymentMethod = "LOAD_COUNT_PAYMENT";
                        break;
                    case 'CHB-Cheque BBVA':
                        namePaymentMethod = "OWN_CHECK_PAYMENT";
                        break;
                    case 'TJC-Tarjeta de crédito':
                        namePaymentMethod = "CREDIT_CARD_PAYMENT";
                        break;
                    case 'CRV-Crédito Virtual':
                        namePaymentMethod = "CREDIT_VIRTUAL_PAYME";
                        break;
                }

                requestPaymentMethods.push({
                    "name": "PAYMENT_METHOD",
                    "isActive": true,
                    "limits": [{
                        "start": "",
                        "end": ""
                        }],
                    "value": [{
                        "name": namePaymentMethod,
                        "id": "S"
                        }]
                });
            });

            // cuenta de espera
            var requestWaitingAccount = {
                "name": "EXTRACT_PAYMENT",
                "isActive": vm.subscriptionFormat == "AD" ? true : false,
                "limits": [{
                    "start": "",
                    "end": ""
                }],
                "value": [{
                    "id": vm.subscriptionFormat == "AD" ? 'AD' : '',
                    "name": vm.subscriptionFormat == "AD" ? 'AD' : '',
                }]
            };

            // Activa Masiva
            var requestMassiveActive = {
                "name": "BULK_ACTIVE",
                "isActive": vm.massiveActive,
                "limits": [{
                    "start": "",
                    "end": ""
                }],
                "value": [{
                    "id": "",
                    "name": ""
                }]
            };

            // inhouse
            var requestInHouse = {
                "name": "IN_HOUSE",
                "isActive": vm.inhouse,
                "limits": [{
                    "start": "",
                    "end": ""
                }],
                "value": [{
                    "id": "",
                    "name": ""
                }]
            };

            // consecutivo
            var requestConsecutive = {
                "name": "CONSECUTIVE",
                "isActive": vm.consecutive,
                "limits": [{
                    "start": "",
                    "end": ""
                }],
                "value": [{
                    "id": "",
                    "name": ""
                }]
            };

            //multilote
            var requestMultilote = {
                "name": "MULTILOTE",
                "isActive": vm.multilote,
                "limits": [{
                    "start": "",
                    "end": ""
                }],
                "value": [{
                    "id": "",
                    "name": ""
                }]
            };

            //trafico de documentos
            var requestDocumentTraffic = {
                "name": "TRAFFIC_DOCUMENTS",
                "isActive": vm.documentTraffic,
                "limits": [{
                    "start": "",
                    "end": ""
                }],
                "value": [{
                    "id": "",
                    "name": ""
                }]
            };

            //webservices

            var requestWebService = {
                "name": "WS_UNIFICATION",
                "isActive": vm.ucw,
                "limits": [{
                    "start": "",
                    "end": ""
                    }],
                "value": [{
                    "id": vm.ucw ? vm.ucwCode : "",
                    "name": vm.ucw ? vm.ucwCode : ""
                    }]
            };

            //asociacion de convenio
            var dataIndicators = [];
            angular.forEach(vm.listStatusAssociatedAgreements, function (value, key) {
                var data = value.associatedAgreementCode + '-' + value.numberAssociatedAgreement + '-' + value.associatedAccount;
                dataIndicators.push({
                    "name": "ASSOCIATE_AGREEMENT",
                    "isActive": vm.associatedAgreements,
                    "limits": [{
                        "start": "",
                        "end": ""
                    }],
                    "value": [{
                        "id": "",
                        "name": vm.associatedAgreements ? data : ''
                    }]
                });
            });

            vm.myPromise = GeneralDataService.createAgreement(requestAgreement, stateIdAgrement)
                .then(function (response) {
                    CreateAgreementService.setIdAgreement(response.data.idAgreement);
                    toastr.info('Solicitud creada con exito.<br> Solicitud : ' + response.data.idAgreement, 'Informacion !');
                    return GeneralDataService.createIndicatorListGneral(requestPaymentMethods, 'PAYMENT_METHOD');
                }).then(function (response) {
                    return GeneralDataService.createIndicatorGeneral(requestWaitingAccount, 'EXTRACT_PAYMENT');
                }).then(function (response) {
                    return GeneralDataService.createIndicatorGeneral(requestMassiveActive, 'BULK_ACTIVE');
                    //   }).then(function (response) {
                    //        return GeneralDataService.createIndicatorGeneral(requestInHouse, 'IN_HOUSE');
                }).then(function (response) {
                    return GeneralDataService.createIndicatorGeneral(requestConsecutive, 'CONSECUTIVE');
                }).then(function (response) {
                    return GeneralDataService.createIndicatorGeneral(requestMultilote, 'MULTILOTE');
                }).then(function (response) {
                    return GeneralDataService.createIndicatorGeneral(requestDocumentTraffic, 'TRAFFIC_DOCUMENTS');
                }).then(function (response) {
                    return GeneralDataService.createIndicatorGeneral(requestWebService, 'WS_UNIFICATION');
                    //}).then(function (response) {
                    //    return GeneralDataService.createIndicatorListGneral(dataIndicators, 'ASSOCIATE_AGREEMENT');
                }).then(function () {
                    console.log("exito");
                }).catch(function (error) {
                    console.log("error: ", error);
                    //toastr.error('Tu usuario o contraseña es incorrecta.', 'Error');
                    // $state.go('templateAuth.home');
                });


        }
    }
})();
