(function () {
    'use strict';

    angular
        .module('app.aplicacion.components.dataBase')
        .controller('OutputFileController', OutputFileController);

    function OutputFileController($scope, CreateAgreementService) {
        var vm = this;

        vm.typeRequest = CreateAgreementService.getTypeRequest();
        vm.disable = false;
        vm.statusSpecialAgreement = true;
        vm.statusInputPositionBd = false;
        vm.statusAutoSendFile = false;
        vm.automaticFiles = [];
        var automaticFile = {
            email: '',
            secureEmail: '',
            compressedType: '',
            emailNotification: ''
        };
        vm.automaticFiles.push(automaticFile);

        vm.addAutomaticFile = addAutomaticFile;
        vm.deleteAutomaticFile = deleteAutomaticFile;

        if (vm.typeRequest == 'R') {
            vm.isCollection = true;
        } else if (vm.typeRequest == 'I') {
            vm.isCollection = false;
        }

        vm.outputFileTypes = [
            {
                nombre: "ESTANDAR BBVA",
                value: "01"
            },
            {
                nombre: "ASOBANCARIA 1998",
                value: "02"
            },
            {
                nombre: "ASOBANCARIA 2000",
                value: "03"
            },
            {
                nombre: "ASOBANCARIA 2001",
                value: "04"
            },
            {
                nombre: "ASOBANCARIA 2011",
                value: "09"
            }
        ];

        vm.formatTypes = [
            {
                nombre: "ERNAL",
                value: "01"
            },
            {
                nombre: "ERECA",
                value: "02"
            }
        ];

        vm.periodicityReports = [
            {
                nombre: "Diario",
                value: "Diario"
            },
            {
                nombre: "Mensual",
                value: "Mensual"
            },
            {
                nombre: "Ambos",
                value: "Ambos"
            },
            {
                nombre: "Ninguno",
                value: "Ninguno"
            }
        ];

        vm.periodicityFiles = [
            {
                nombre: "Anual",
                value: "A"
            },
            {
                nombre: "Mensual",
                value: "M"
            },
            {
                nombre: "Diario (Automatico)",
                value: "D"
            }
        ];

        vm.destinations = [
            {
                nombre: "FTP",
                value: "FTP"
            },
            {
                nombre: "CASH",
                value: "CASH"
            }
        ];

        vm.partialTransfers = [
            {
                nombre: "Acumulado",
                value: "A"
            },
            {
                nombre: "Incremental",
                value: "I"
            },
            {
                nombre: "Parcial",
                value: "P"
            },
            {
                nombre: "Total",
                value: "T"
            }
        ];

        function addAutomaticFile() {
            vm.automaticFiles.push({
                email: '',
                secureEmail: '',
                compressedType: '',
                emailNotification: ''
            });
        }

        function deleteAutomaticFile(index) {
            if (vm.automaticFiles.length > 1) {
                vm.automaticFiles.splice(index, 1);
            } else {
                vm.automaticFiles.splice(index, 1);
                vm.automaticFiles.push({
                    email: '',
                    secureEmail: '',
                    compressedType: '',
                    emailNotification: ''
                });
            }

        }

        $scope.$watch('vm.specialAgreement', function (value) {
            if (vm.specialAgreement == true) {
                vm.statusSpecialAgreement = true;
            } else {
                vm.statusSpecialAgreement = false;
            }
        });

        $scope.$watch('vm.inputPositionBd', function (value) {
            if (vm.inputPositionBd == true) {
                vm.statusInputPositionBd = true;
            } else {
                vm.statusInputPositionBd = false;
            }
        });

        $scope.$watch('vm.autoSendFile', function (value) {
            if (vm.autoSendFile == true) {
                vm.statusAutoSendFile = true;
            } else {
                vm.statusAutoSendFile = false;
            }
        });

        $scope.$watch('vm.fileGeneration', function (value) {
            if (vm.fileGeneration == false) {
                vm.disable = true;
            } else {
                vm.disable = false;
            }
        });
    }
})();
