(function () {
    'use strict';

    angular
        .module('app.aplicacion.components.financialTerminal')
        .controller('FinancialTerminalController', FinancialTerminalController);

    function FinancialTerminalController() {
        var vm = this;

        vm.changeType = changeType;
        vm.statusOffice = false;

        vm.offices = [];

        var oficce = {
            officeCode: ''
        };

        vm.offices.push(oficce);

        vm.addOficce = addOficce;
        vm.deleteOficce = deleteOficce;

        vm.types = [
            {
                nombre: 'Nacional',
                value: 'NAL'
            },
            {
                nombre: 'Oficina',
                value: 'OFI'
            },
            {
                nombre: 'Centro de Servicios',
                value: 'CSR'
            }
        ];

        function changeType() {
            if (vm.type == 'OFI') {
                vm.statusOffice = true;
            } else {
                vm.statusOffice = false;
            }
        }

        function addOficce() {
            var oficce = {
                officeCode: ''
            };

            vm.offices.push(oficce);
        }

        function deleteOficce(index) {
            if (vm.offices.length > 1) {
                vm.offices.splice(index, 1);
            } else {
                vm.offices.splice(index, 1);
                vm.offices.push({
                    officeCode: ''
                });
            }
        }

    }
})();
