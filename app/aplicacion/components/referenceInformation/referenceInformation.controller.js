(function () {
    'use strict';

    angular
        .module('app.aplicacion.components.referenceInformation')
        .controller('ReferenceInformationController', ReferenceInformationController);

    function ReferenceInformationController(CreateAgreementService, toastr) {
        var vm = this;

        vm.typeRequest = CreateAgreementService.getTypeRequest();

        vm.changeType = changeType;

        vm.references = [];
        vm.additionals = [];
        vm.fixedValues = [];
        vm.dates = [];
        vm.values = [];

        if (vm.typeRequest == 'R') {
            vm.isTax = false;
        } else if (vm.typeRequest == 'I') {
            vm.isTax = true;
        }

        var reference = {
            description: '',
            format: '',
            fieldLength: '',
            barLength: '',
            obligatoryField: '',
            inputPosition: 0,
            outputPosition: '',
            fillCharacter: '',
            quickHelp: '',
            alignment: '',
            municipalityField: '',
            taxOver: '',
            routineValidation: ''
        };

        var additional = {
            description: '',
            format: '',
            fieldLength: '',
            obligatoryField: '',
            inputPosition: 0,
            outputPosition: '',
            fillCharacter: '',
            quickHelp: '',
            alignment: '',
            municipalityField: '',
            taxOver: '',
            routineValidation: ''
        };

        var fixedValue = {
            description: '',
            consecutive: '',
            format: '',
            fieldLength: '',
            amountIndicator: '',
            obligatoryField: '',
            minimumAmount: '',
            maximumAmount: ''
        };

        var date = {
            description: '',
            format: '',
            inputPosition: '',
            fieldLength: 8,
            alignment: '',
            fillCharacter: '',
            lengthBars: 8,
            quickHelp: ''
        };

        var value = {
            description: '',
            format: '',
            inputPosition: '',
            fieldLength: '',
            alignment: '',
            fillCharacter: '',
            lengthBars: '',
            quickHelp: ''
        };

        vm.references.push(reference);
        vm.additionals.push(additional);
        vm.fixedValues.push(fixedValue);
        vm.dates.push(date);
        vm.values.push(value);

        vm.addReference = addReference;
        vm.deleteReference = deleteReference;
        vm.addAdditional = addAdditional;
        vm.deleteAdditional = deleteAdditional;
        vm.addFixedValue = addFixedValue;
        vm.deleteFixedValue = deleteFixedValue;
        vm.addDate = addDate;
        vm.deleteDate = deleteDate;
        vm.addValue = addValue;
        vm.deleteValue = deleteValue;
        vm.deleteInformationMessage = deleteInformationMessage;

        vm.stateReference = false;
        vm.stateAdditional = false;
        vm.stateFixedValue = false;
        vm.stateDate = false;
        vm.stateValue = false;
        vm.stateInformationMessage = false;

        vm.types = [
            {
                nombre: 'Referencia',
                value: 'REF'
            },
            {
                nombre: 'Adicional',
                value: 'ADI'
            },
            {
                nombre: 'Valor Fijo',
                value: 'VAF'
            },
            {
                nombre: 'Fecha',
                value: 'FEC'
            },
            {
                nombre: 'Valor',
                value: 'VAL'
            },
            {
                nombre: 'Mensaje',
                value: 'MSJ'
            }
        ];

        vm.referenceFormats = [
            {
                nombre: 'Si',
                value: 'true'
            },
            {
                nombre: 'No',
                value: 'false'
            }
        ];

        vm.referenceObligatoryFields = [
            {
                nombre: 'AlfaNúmerico',
                value: 'A'
            },
            {
                nombre: 'Númerico',
                value: 'N'
            },
            {
                nombre: 'Fecha (AAAAMMDD)',
                value: 'D1'
            },
            {
                nombre: 'Fecha (DDMMAAAA)',
                value: 'D2'
            }
        ];

        vm.referenceFillCharacters = [
            {
                nombre: 'Ceros',
                value: 'ZEROS'
            },
            {
                nombre: 'Espacios',
                value: 'SPACES'
            }
        ];

        vm.referenceAlignments = [
            {
                nombre: 'A la derecha',
                value: 'D'
            },
            {
                nombre: 'A la Izquierda',
                value: 'I'
            }
        ];

        vm.referenceRoutineValidations = [
            {
                nombre: 'Módulo 06',
                value: 'MO06'
            },
            {
                nombre: 'Módulo 10',
                value: 'MO10'
            },
            {
                nombre: 'Módulo 11',
                value: 'MO11'
            },
            {
                nombre: 'Módulo 15',
                value: 'MO15'
            },
            {
                nombre: 'Modo Doble Dígito',
                value: 'DODI'
            }
        ];

        vm.fixedValueFormats = [
            {
                nombre: 'AlfaNúmerico',
                value: 'A'
            },
            {
                nombre: 'Númerico',
                value: 'N'
            },
            {
                nombre: 'Fecha (AAAAMMDD)',
                value: 'D1'
            },
            {
                nombre: 'Fecha (DDMMAAAA)',
                value: 'D2'
            }
        ];

        vm.dateFormats = [
            {
                nombre: 'AlfaNúmerico',
                value: 'A'
            },
            {
                nombre: 'Númerico',
                value: 'N'
            },
            {
                nombre: 'Fecha (AAAAMMDD)',
                value: 'D1'
            },
            {
                nombre: 'Fecha (DDMMAAAA)',
                value: 'D2'
            }
        ];

        vm.dateAlignments = [
            {
                nombre: 'A la derecha',
                value: 'D'
            },
            {
                nombre: 'A la Izquierda',
                value: 'I'
            }
        ];

        vm.dateFillCharacters = [
            {
                nombre: 'Ceros',
                value: 'ZEROS'
            },
            {
                nombre: 'Espacios',
                value: 'SPACES'
            }
        ];

        vm.valueFormats = [
            {
                nombre: 'AlfaNúmerico',
                value: 'A'
            },
            {
                nombre: 'Númerico',
                value: 'N'
            },
            {
                nombre: 'Fecha (AAAAMMDD)',
                value: 'D1'
            },
            {
                nombre: 'Fecha (DDMMAAAA)',
                value: 'D2'
            }
        ];

        vm.valueAlignments = [
            {
                nombre: 'A la derecha',
                value: 'D'
            },
            {
                nombre: 'A la Izquierda',
                value: 'I'
            }
        ];

        vm.valueFillCharacters = [
            {
                nombre: 'Ceros',
                value: 'ZEROS'
            },
            {
                nombre: 'Espacios',
                value: 'SPACES'
            }
        ];

        function changeType() {
            if (vm.type == '') {
                vm.stateReference = false;
                vm.stateAdditional = false;
                vm.stateFixedValue = false;
                vm.stateDate = false;
                vm.stateValue = false;
                vm.stateInformationMessage = false;
            } else if (vm.type == 'REF') {
                vm.stateReference = true;
                vm.stateAdditional = false;
                vm.stateFixedValue = false;
                vm.stateDate = false;
                vm.stateValue = false;
                vm.stateInformationMessage = false;
            } else if (vm.type == 'ADI') {
                vm.stateAdditional = true;
                vm.stateReference = false;
                vm.stateFixedValue = false;
                vm.stateDate = false;
                vm.stateValue = false;
                vm.stateInformationMessage = false;
            } else if (vm.type == 'VAF') {
                vm.stateFixedValue = true;
                vm.stateReference = false;
                vm.stateAdditional = false;
                vm.stateDate = false;
                vm.stateValue = false;
                vm.stateInformationMessage = false;
            } else if (vm.type == 'FEC') {
                vm.stateDate = true;
                vm.stateFixedValue = false;
                vm.stateReference = false;
                vm.stateAdditional = false;
                vm.stateValue = false;
                vm.stateInformationMessage = false;
            } else if (vm.type == 'VAL') {
                vm.stateValue = true;
                vm.stateDate = false;
                vm.stateFixedValue = false;
                vm.stateReference = false;
                vm.stateAdditional = false;
                vm.stateInformationMessage = false;
            } else if (vm.type == 'MSJ') {
                vm.stateInformationMessage = true;
                vm.stateValue = false;
                vm.stateDate = false;
                vm.stateFixedValue = false;
                vm.stateReference = false;
                vm.stateAdditional = false;
            }
        }

        function addReference() {

            if (vm.references.length <= 6) {
                var reference = {
                    description: '',
                    format: '',
                    fieldLength: '',
                    barLength: '',
                    obligatoryField: '',
                    inputPosition: 0,
                    outputPosition: '',
                    fillCharacter: '',
                    quickHelp: '',
                    alignment: '',
                    municipalityField: '',
                    taxOver: '',
                    routineValidation: ''
                };
                vm.references.push(reference);
            } else {
                toastr.error('Limite de referencias excedido', 'Error');
            }
        }

        function deleteReference(index) {
            if (vm.references.length > 1) {
                vm.references.splice(index, 1);
            } else {
                vm.references.splice(index, 1);
                vm.references.push({
                    description: '',
                    format: '',
                    fieldLength: '',
                    barLength: '',
                    obligatoryField: '',
                    inputPosition: 0,
                    outputPosition: '',
                    fillCharacter: '',
                    quickHelp: '',
                    alignment: '',
                    municipalityField: '',
                    taxOver: '',
                    routineValidation: ''
                });
            }
        }

        function addAdditional() {

            if (vm.additional.length <= 6) {
                var additional = {
                    description: '',
                    format: '',
                    fieldLength: '',
                    obligatoryField: '',
                    inputPosition: 0,
                    outputPosition: '',
                    fillCharacter: '',
                    quickHelp: '',
                    alignment: '',
                    municipalityField: '',
                    taxOver: '',
                    routineValidation: ''
                };
                vm.additionals.push(additional);
            } else {
                toastr.error('Limite de referencias adicionales excedido', 'Error');
            }
        }

        function deleteAdditional(index) {
            if (vm.additionals.length > 1) {
                vm.additionals.splice(index, 1);
            } else {
                vm.additionals.splice(index, 1);
                vm.additionals.push({
                    description: '',
                    format: '',
                    fieldLength: '',
                    obligatoryField: '',
                    inputPosition: 0,
                    outputPosition: '',
                    fillCharacter: '',
                    quickHelp: '',
                    alignment: '',
                    municipalityField: '',
                    taxOver: '',
                    routineValidation: ''
                });
            }
        }

        function addFixedValue() {
            var fixedValue = {
                description: '',
                consecutive: '',
                format: '',
                fieldLength: '',
                amountIndicator: '',
                obligatoryField: '',
                minimumAmount: '',
                maximumAmount: ''
            };
            vm.fixedValues.push(fixedValue);
        }

        function deleteFixedValue(index) {
            if (vm.fixedValues.length > 1) {
                vm.fixedValues.splice(index, 1);
            } else {
                vm.fixedValues.splice(index, 1);
                vm.fixedValues.push({
                    description: '',
                    consecutive: '',
                    format: '',
                    fieldLength: '',
                    amountIndicator: '',
                    obligatoryField: '',
                    minimumAmount: '',
                    maximumAmount: ''
                });
            }
        }

        function addDate() {
            var date = {
                description: '',
                format: '',
                inputPosition: '',
                fieldLength: 8,
                alignment: '',
                fillCharacter: '',
                lengthBars: 8,
                quickHelp: ''
            };

            vm.dates.push(date);
        }

        function deleteDate(index) {
            if (vm.dates.length > 1) {
                vm.dates.splice(index, 1);
            } else {
                vm.dates.splice(index, 1);
                vm.dates.push({
                    description: '',
                    format: '',
                    inputPosition: '',
                    fieldLength: 8,
                    alignment: '',
                    fillCharacter: '',
                    lengthBars: 8,
                    quickHelp: ''
                });
            }
        }

        function addValue() {
            var value = {
                description: '',
                format: '',
                inputPosition: '',
                fieldLength: '',
                alignment: '',
                fillCharacter: '',
                lengthBars: '',
                quickHelp: ''
            };

            vm.values.push(value);
        }

        function deleteValue(index) {
            if (vm.values.length > 1) {
                vm.values.splice(index, 1);
            } else {
                vm.values.splice(index, 1);
                vm.values.push({
                    description: '',
                    format: '',
                    inputPosition: '',
                    fieldLength: '',
                    alignment: '',
                    fillCharacter: '',
                    lengthBars: '',
                    quickHelp: ''
                });
            }
        }

        function deleteInformationMessage() {
            vm.informationMessage = '';
        }
    }
})();
